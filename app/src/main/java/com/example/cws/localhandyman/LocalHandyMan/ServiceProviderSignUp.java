package com.example.cws.localhandyman.LocalHandyMan;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cws.localhandyman.R;

public class ServiceProviderSignUp extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
EditText service_provider_name,service_provider_email,service_provider_password,service_provider_confirmpassword,service_provider_phone,service_provider_businessname,service_provider_zipcode;
    TextView signup,spinnertextview1,spinnertextview2;
    Spinner spinner1,spinner2,spinner3;
    String[] radius={"2km","5km","8km","10km","15km","20km","25km","30km","35km","40km","40+ km"};
    String[] maincategory={"Selecte Category ","Automotive Repair","Legal","HouseHold Services","Personal & More","Tutors","Real Estate Service","Business Service","Repair & Maintenance","Lession & Hobbies","Events & Wedding"};
    String [] automotive_subcategor={"Car Cleaning & Servicing","Car Repair & Denting","Driver On Demand"};
    String[] legal_subcategory={"Divorce Lawyer","Public Lawyer","Real Estate Lawyer","Visa Agency"};
    String[] household_subcategory={"AC Repair","Mobile Repair","Architect","Carpenter","Chimney and Hob Repair","Electrician","House Painters","Modular kitchen","Laptop Repair","Microwave Repair","Pest Control","Plumber","Bathroom Deep Cleaning","Carpet Cleaning","Home Deep Cleaning","Refrigerator Repair","RO & Water Purifier Repair","Water Proofing","Television Repair","Washing Repair","Furniture Repair","Ladies Salon At Home", "Men Salon At Home","Birthday Party Planner","Wedding Planner","Mover's & Packer's"};
    String[] personel_subcategory={"Monthly Tiffin Services","Portfolio & Photography"};
    String [] tutors_subcategory={"Spanish Lession","French Lession","Classical Dance & Music Lession","Sign Language","Musical Instrument Lession","Home Tutor Upto 12 Class"};
    String [] realestate_subcategory={"Buy New & Old Home","Room On Rent Services"};
    String [] repairmaintenance_subcategory={"Home Repair And Contruction"};
    String [] businessservice_subcategory={"CA For Small Business","CCTV Cameras Installation","CA/CS For Company Registration","Corporate Party Planner","Insurance Agent","Web Designer & Development"};
    String [] lession_subcategory={"Drum Classes","Guitar Classes","KeyBoard Classes","Salsa Dance Class","Zumba Fitness Dance","Hip Pop Dance Classes"};
    String [] event_subcategory={"Mehendi Artists","Corporate Event Planer","DJ","Live Musician","Pre Wedding Shoot","Wedding Caterers","Bridal Makeup Artist","Wedding Photographer"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_provider_sign_up);
        service_provider_name=(EditText)findViewById(R.id.service_provider_name);
        service_provider_email=(EditText)findViewById(R.id.service_provider_email);
        service_provider_password=(EditText)findViewById(R.id.service_provider_password);
        service_provider_confirmpassword=(EditText)findViewById(R.id.service_provider_confirmpassword);
        service_provider_businessname=(EditText)findViewById(R.id.service_provider_businessname);
        service_provider_phone=(EditText)findViewById(R.id.service_provider_phone);
        service_provider_zipcode=(EditText)findViewById(R.id.service_provider_zipcode);
        spinner1=(Spinner)findViewById(R.id.spinner1);
        ArrayAdapter arrayAdapter=new ArrayAdapter(ServiceProviderSignUp.this,R.layout.support_simple_spinner_dropdown_item,maincategory);
        spinner1.setAdapter(arrayAdapter);
        spinner1.setOnItemSelectedListener(ServiceProviderSignUp.this);
        spinner2=(Spinner)findViewById(R.id.spinner2);
        spinner3=(Spinner)findViewById(R.id.spinner3);
        ArrayAdapter radiusAdapter=new ArrayAdapter(ServiceProviderSignUp.this,R.layout.support_simple_spinner_dropdown_item,radius);
        spinner3.setAdapter(radiusAdapter);
       spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

           }

           @Override
           public void onNothingSelected(AdapterView<?> parent) {

           }
       });
        signup=(TextView)findViewById(R.id.signupbtn);
        spinnertextview2=(TextView)findViewById(R.id.spinnertextview2);
        spinnertextview1=(TextView)findViewById(R.id.spinnertextview1);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        Log.d("i",""+i);
        switch (i)
        {
            case 0:
                spinner2.setVisibility(View.GONE);
                spinnertextview2.setVisibility(View.GONE);
                break;
            case 1:
                ArrayAdapter subarray_automotive=new ArrayAdapter(ServiceProviderSignUp.this,R.layout.support_simple_spinner_dropdown_item,automotive_subcategor);
                spinner2.setAdapter(subarray_automotive);
                subarray_automotive.notifyDataSetChanged();
                spinner2.setVisibility(View.VISIBLE);
                spinnertextview2.setVisibility(View.VISIBLE);
                break;
            case 2:
                ArrayAdapter subarray_legal=new ArrayAdapter(ServiceProviderSignUp.this,R.layout.support_simple_spinner_dropdown_item,legal_subcategory);
                spinner2.setAdapter(subarray_legal);
                subarray_legal.notifyDataSetChanged();
                spinner2.setVisibility(View.VISIBLE);
                spinnertextview2.setVisibility(View.VISIBLE);
                break;
            case 3:
                ArrayAdapter subarray_household=new ArrayAdapter(ServiceProviderSignUp.this,R.layout.support_simple_spinner_dropdown_item,household_subcategory);
                spinner2.setAdapter(subarray_household);
                subarray_household.notifyDataSetChanged();
                spinner2.setVisibility(View.VISIBLE);
                spinnertextview2.setVisibility(View.VISIBLE);
                break;
            case 4:
                ArrayAdapter subarray_personal=new ArrayAdapter(ServiceProviderSignUp.this,R.layout.support_simple_spinner_dropdown_item,personel_subcategory);
                spinner2.setAdapter(subarray_personal);
                subarray_personal.notifyDataSetChanged();
                spinner2.setVisibility(View.VISIBLE);
                spinnertextview2.setVisibility(View.VISIBLE);
                break;
            case 5:
                ArrayAdapter subarray_tutors=new ArrayAdapter(ServiceProviderSignUp.this,R.layout.support_simple_spinner_dropdown_item,tutors_subcategory);
                spinner2.setAdapter(subarray_tutors);
                subarray_tutors.notifyDataSetChanged();
                spinner2.setVisibility(View.VISIBLE);
                spinnertextview2.setVisibility(View.VISIBLE);
                break;
            case 6:
                ArrayAdapter subarray_realestate=new ArrayAdapter(ServiceProviderSignUp.this,R.layout.support_simple_spinner_dropdown_item,realestate_subcategory);
                spinner2.setAdapter(subarray_realestate);
                subarray_realestate.notifyDataSetChanged();
                spinner2.setVisibility(View.VISIBLE);
                spinnertextview2.setVisibility(View.VISIBLE);
                break;
            case 7:
                ArrayAdapter subarray_businessservice=new ArrayAdapter(ServiceProviderSignUp.this,R.layout.support_simple_spinner_dropdown_item,businessservice_subcategory);
                spinner2.setAdapter(subarray_businessservice);
                subarray_businessservice.notifyDataSetChanged();
                spinner2.setVisibility(View.VISIBLE);
                spinnertextview2.setVisibility(View.VISIBLE);
                break;
            case 8:
                ArrayAdapter subarray_repairmaintenance=new ArrayAdapter(ServiceProviderSignUp.this,R.layout.support_simple_spinner_dropdown_item,repairmaintenance_subcategory);
                spinner2.setAdapter(subarray_repairmaintenance);
                subarray_repairmaintenance.notifyDataSetChanged();
                spinner2.setVisibility(View.VISIBLE);
                spinnertextview2.setVisibility(View.VISIBLE);
                break;
            case 9:
                ArrayAdapter subarray_lessionhobbies=new ArrayAdapter(ServiceProviderSignUp.this,R.layout.support_simple_spinner_dropdown_item,lession_subcategory);
                spinner2.setAdapter(subarray_lessionhobbies);
                subarray_lessionhobbies.notifyDataSetChanged();
                spinner2.setVisibility(View.VISIBLE);
                spinnertextview2.setVisibility(View.VISIBLE);
                break;
            case 10:
                ArrayAdapter subarray_eventswedding=new ArrayAdapter(ServiceProviderSignUp.this,R.layout.support_simple_spinner_dropdown_item,event_subcategory);
                spinner2.setAdapter(subarray_eventswedding);
                subarray_eventswedding.notifyDataSetChanged();
                spinner2.setVisibility(View.VISIBLE);
                spinnertextview2.setVisibility(View.VISIBLE);
                break;
            default:
                spinner2.setVisibility(View.GONE);
                spinnertextview2.setVisibility(View.GONE);
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        Toast.makeText(this, "Nothing selected", Toast.LENGTH_SHORT).show();
    }
}
