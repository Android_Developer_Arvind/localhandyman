package com.example.cws.localhandyman.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cws.localhandyman.R;

import java.util.ArrayList;

/**
 * Created by cws on 5/4/18.
 */

public class ListViewAdapter extends BaseAdapter {
ArrayList <String> dataitemlist;
    ArrayList<Drawable> iconlist;
    LayoutInflater layoutInflater;
Context context;
    public ListViewAdapter(Context context,ArrayList<String> dataitemlist,ArrayList<Drawable> iconlist) {
        this.context=context;
        this.dataitemlist=dataitemlist;
        this.iconlist=iconlist;

    }

    @Override
    public int getCount() {
        return dataitemlist.size();
    }

    @Override
    public Object getItem(int position) {
        return dataitemlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView=LayoutInflater.from(context).inflate(R.layout.customerdrawerlayout,parent,false);
        ImageView imageView=(ImageView)convertView.findViewById(R.id.imageview);
        TextView textView=(TextView)convertView.findViewById(R.id.textview);
        textView.setText(dataitemlist.get(position));
        imageView.setImageDrawable(iconlist.get(position));

        return convertView;
    }
}
