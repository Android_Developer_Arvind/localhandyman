package com.example.cws.localhandyman.LocalHandyMan;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cws.localhandyman.ConstantHelperMethods.HelperClass;
import com.example.cws.localhandyman.R;

public class CustomerSignUp extends AppCompatActivity {
EditText customername,customeremail,customerpassword,customerconfirmpassword,customerphone;
    String name,email,password,confirmpassword,phone;
    //int phone;
    boolean validphone,validemail;
    TextView signupbtn;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_sign_up);
        customername=(EditText)findViewById(R.id.customer_name);
        customeremail=(EditText)findViewById(R.id.customer_email);
        customerpassword=(EditText)findViewById(R.id.customer_password);
        customerconfirmpassword=(EditText)findViewById(R.id.customer_confirmpassword);
        customerphone=(EditText)findViewById(R.id.customer_phone);
        signupbtn=(TextView)findViewById(R.id.signupbtn);
        signupbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name=customername.getText().toString().trim();
                email=customeremail.getText().toString().trim();
                password=customerpassword.getText().toString().trim();
                confirmpassword=customerconfirmpassword.getText().toString().trim();
                phone=customerphone.getText().toString().trim();
                Log.e("name",name);
                Log.e("email",email);
                Log.e("passwd",password);
                Log.e("cpswd",confirmpassword);
                Log.e("phone",phone);

               if( !((customername.getText().toString().trim().equals("") || customeremail.getText().toString().trim().equals("") || customerpassword.getText().toString().trim().equals("")) || (customerconfirmpassword.getText().toString().trim().equals("") || customerphone.getText().toString().trim().equals(""))))
                {
                 validemail= HelperClass.isValidEmail(email);
                 validphone=HelperClass.isValidPhone(phone);
                  if(password.equals(confirmpassword))
                  {
                      if(validemail && validphone && !(phone.length()<10))
                      {
                          Toast.makeText(CustomerSignUp.this, "Data is Fine", Toast.LENGTH_SHORT).show();
                          saveUserData();
                          Intent i=new Intent(CustomerSignUp.this,CustomerHomeScreenActivity.class);
                          startActivity(i);
                      }
                      else
                      {
                          Toast.makeText(CustomerSignUp.this, "Check, InValid Email or Phone", Toast.LENGTH_SHORT).show();
                      }
                  }
                  else
                  {
                      Toast.makeText(CustomerSignUp.this, "Password Doesn't Match", Toast.LENGTH_SHORT).show();
                  }


                }
                else
                {
                    Toast.makeText(CustomerSignUp.this, "Please Fill Details First", Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

    public void saveUserData()
    {
        sharedPreferences=getSharedPreferences("User_Detail",0);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("name",name);
        editor.putString("email",email);
        editor.putString("password",password);
        editor.putString("confirmpassword",confirmpassword);
        editor.putString("phone",phone);
        editor.commit();
    }


}


