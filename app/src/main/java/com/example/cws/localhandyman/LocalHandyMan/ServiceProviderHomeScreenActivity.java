package com.example.cws.localhandyman.LocalHandyMan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cws.localhandyman.R;

public class ServiceProviderHomeScreenActivity extends AppCompatActivity implements View.OnClickListener {
    TextView signup,forgetpassword;
    EditText email,password;
    TextView loginbtn;
    ImageView fblogin,gmaillogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_provider_home_screen);
        email=(EditText)findViewById(R.id.customer_email);
        password=(EditText)findViewById(R.id.customer_password);
        loginbtn=(TextView)findViewById(R.id.loginbtn);
        fblogin=(ImageView)findViewById(R.id.facebookimage);
        gmaillogin=(ImageView)findViewById(R.id.gmailimage);
        signup=(TextView)findViewById(R.id.signupbtn);
        forgetpassword=(TextView)findViewById(R.id.forgetpassword);
        signup.setOnClickListener(this);
       forgetpassword.setOnClickListener(this);
        loginbtn.setOnClickListener(this);
        fblogin.setOnClickListener(this);
        gmaillogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
 switch(view.getId())
 {
     case R.id.loginbtn:
         Toast.makeText(this, "Please Wait", Toast.LENGTH_SHORT).show();
         break;
     case R.id.facebookimage:
         Toast.makeText(this, "Please Wait", Toast.LENGTH_SHORT).show();
         break;
     case  R.id.gmailimage:
         Toast.makeText(this, "Please Wait", Toast.LENGTH_SHORT).show();
         break;
     case R.id.signupbtn:
         Intent i=new Intent(ServiceProviderHomeScreenActivity.this,ServiceProviderSignUp.class);
         startActivity(i);
         break;
     case  R.id.forgetpassword:
         Intent i1=new Intent(ServiceProviderHomeScreenActivity.this,ForgetPassword.class);
         startActivity(i1);
         break;
 }
    }
}
