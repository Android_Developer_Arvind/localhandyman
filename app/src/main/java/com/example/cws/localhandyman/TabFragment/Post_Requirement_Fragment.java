package com.example.cws.localhandyman.TabFragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.cws.localhandyman.LocalHandyMan.ServiceProviderSignUp;
import com.example.cws.localhandyman.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Post_Requirement_Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Post_Requirement_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Post_Requirement_Fragment extends Fragment implements AdapterView.OnItemSelectedListener  {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Spinner spinner1,spinner2;
    EditText commentedtext;
    Button submit;
    String[] radius={"2km","5km","8km","10km","15km","20km","25km","30km","35km","40km","40+ km"};
    String[] maincategory={"Selecte Category ","Automotive Repair","Legal Services","HouseHold Services","Personal & More","Tutors Services","Real Estate Service","Business Service","Repair & Maintenance","Lession & Hobbies","Events & Wedding"};
    String [] automotive_subcategor={"Car Cleaning & Servicing","Car Repair & Denting","Driver On Demand"};
    String[] legal_subcategory={"Divorce Lawyer","Public Lawyer","Real Estate Lawyer","Visa Agency"};
    String[] household_subcategory={"AC Repair","Mobile Repair","Architect","Carpenter","Chimney and Hob Repair","Electrician","House Painters","Modular kitchen","Laptop Repair","Microwave Repair","Pest Control","Plumber","Bathroom Deep Cleaning","Carpet Cleaning","Home Deep Cleaning","Refrigerator Repair","RO & Water Purifier Repair","Water Proofing","Television Repair","Washing Repair","Furniture Repair","Ladies Salon At Home", "Men Salon At Home","Birthday Party Planner","Wedding Planner","Mover's & Packer's"};
    String[] personel_subcategory={"Monthly Tiffin Services","Portfolio & Photography"};
    String [] tutors_subcategory={"Spanish Lession","French Lession","Classical Dance & Music Lession","Sign Language","Musical Instrument Lession","Home Tutor Upto 12 Class"};
    String [] realestate_subcategory={"Buy New & Old Home","Room On Rent Services"};
    String [] repairmaintenance_subcategory={"Home Repair And Contruction"};
    String [] businessservice_subcategory={"CA For Small Business","CCTV Cameras Installation","CA/CS For Company Registration","Corporate Party Planner","Insurance Agent","Web Designer & Development"};
    String [] lession_subcategory={"Drum Classes","Guitar Classes","KeyBoard Classes","Salsa Dance Class","Zumba Fitness Dance","Hip Pop Dance Classes"};
    String [] event_subcategory={"Mehendi Artists","Corporate Event Planer","DJ","Live Musician","Pre Wedding Shoot","Wedding Caterers","Bridal Makeup Artist","Wedding Photographer"};
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Post_Requirement_Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Post_Requirement_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Post_Requirement_Fragment newInstance(String param1, String param2) {
        Post_Requirement_Fragment fragment = new Post_Requirement_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_post__requirement_, container, false);
        spinner1=(Spinner)v.findViewById(R.id.spinner1);
        ArrayAdapter arrayAdapter=new ArrayAdapter(getActivity(),R.layout.support_simple_spinner_dropdown_item,maincategory);
        spinner1.setAdapter(arrayAdapter);
        submit=(Button)v.findViewById(R.id.submit);
        commentedtext=(EditText)v.findViewById(R.id.commentedtext);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Please Wait ", Toast.LENGTH_SHORT).show();
            }
        });
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (position)
                {
                    case 0:
                        spinner2.setVisibility(View.GONE);
                        // spinnertextview2.setVisibility(View.GONE);
                        break;
                    case 1:
                        ArrayAdapter subarray_automotive=new ArrayAdapter(getActivity(),R.layout.support_simple_spinner_dropdown_item,automotive_subcategor);
                        spinner2.setAdapter(subarray_automotive);
                        subarray_automotive.notifyDataSetChanged();
                        spinner2.setVisibility(View.VISIBLE);
                        // spinnertextview2.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        ArrayAdapter subarray_legal=new ArrayAdapter(getActivity(),R.layout.support_simple_spinner_dropdown_item,legal_subcategory);
                        spinner2.setAdapter(subarray_legal);
                        subarray_legal.notifyDataSetChanged();
                        spinner2.setVisibility(View.VISIBLE);
                        //spinnertextview2.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        ArrayAdapter subarray_household=new ArrayAdapter(getActivity(),R.layout.support_simple_spinner_dropdown_item,household_subcategory);
                        spinner2.setAdapter(subarray_household);
                        subarray_household.notifyDataSetChanged();
                        spinner2.setVisibility(View.VISIBLE);
                        //spinnertextview2.setVisibility(View.VISIBLE);
                        break;
                    case 4:
                        ArrayAdapter subarray_personal=new ArrayAdapter(getActivity(),R.layout.support_simple_spinner_dropdown_item,personel_subcategory);
                        spinner2.setAdapter(subarray_personal);
                        subarray_personal.notifyDataSetChanged();
                        spinner2.setVisibility(View.VISIBLE);
                        //spinnertextview2.setVisibility(View.VISIBLE);
                        break;
                    case 5:
                        ArrayAdapter subarray_tutors=new ArrayAdapter(getActivity(),R.layout.support_simple_spinner_dropdown_item,tutors_subcategory);
                        spinner2.setAdapter(subarray_tutors);
                        subarray_tutors.notifyDataSetChanged();
                        spinner2.setVisibility(View.VISIBLE);
                        // spinnertextview2.setVisibility(View.VISIBLE);
                        break;
                    case 6:
                        ArrayAdapter subarray_realestate=new ArrayAdapter(getActivity(),R.layout.support_simple_spinner_dropdown_item,realestate_subcategory);
                        spinner2.setAdapter(subarray_realestate);
                        subarray_realestate.notifyDataSetChanged();
                        spinner2.setVisibility(View.VISIBLE);
                        // spinnertextview2.setVisibility(View.VISIBLE);
                        break;
                    case 7:
                        ArrayAdapter subarray_businessservice=new ArrayAdapter(getActivity(),R.layout.support_simple_spinner_dropdown_item,businessservice_subcategory);
                        spinner2.setAdapter(subarray_businessservice);
                        subarray_businessservice.notifyDataSetChanged();
                        spinner2.setVisibility(View.VISIBLE);
                        //spinnertextview2.setVisibility(View.VISIBLE);
                        break;
                    case 8:
                        ArrayAdapter subarray_repairmaintenance=new ArrayAdapter(getActivity(),R.layout.support_simple_spinner_dropdown_item,repairmaintenance_subcategory);
                        spinner2.setAdapter(subarray_repairmaintenance);
                        subarray_repairmaintenance.notifyDataSetChanged();
                        spinner2.setVisibility(View.VISIBLE);
                        //spinnertextview2.setVisibility(View.VISIBLE);
                        break;
                    case 9:
                        ArrayAdapter subarray_lessionhobbies=new ArrayAdapter(getActivity(),R.layout.support_simple_spinner_dropdown_item,lession_subcategory);
                        spinner2.setAdapter(subarray_lessionhobbies);
                        subarray_lessionhobbies.notifyDataSetChanged();
                        spinner2.setVisibility(View.VISIBLE);
                        //spinnertextview2.setVisibility(View.VISIBLE);
                        break;
                    case 10:
                        ArrayAdapter subarray_eventswedding=new ArrayAdapter(getActivity(),R.layout.support_simple_spinner_dropdown_item,event_subcategory);
                        spinner2.setAdapter(subarray_eventswedding);
                        subarray_eventswedding.notifyDataSetChanged();
                        spinner2.setVisibility(View.VISIBLE);
                        //spinnertextview2.setVisibility(View.VISIBLE);
                        break;
                    default:
                        spinner2.setVisibility(View.GONE);
                        //spinnertextview2.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner2=(Spinner)v.findViewById(R.id.spinner2);
        return  v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Log.d("i",""+position);
        Log.d("view",""+view);

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
