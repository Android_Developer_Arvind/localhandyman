package com.example.cws.localhandyman.TabFragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.cws.localhandyman.Adapter.CustomerTabFragmentAdapter;
import com.example.cws.localhandyman.R;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SubCategory_Automotive_Repair_Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SubCategory_Automotive_Repair_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SubCategory_Automotive_Repair_Fragment extends Fragment  {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
   // ArrayList<String[]> list=new ArrayList<>();
    String[] autosublist;
    ListView listView;
    int mainindex;
    boolean listviewclicked=false;
    ArrayAdapter arrayAdapter;
    TabLayout.Tab tab;
    TabLayout tablayout;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    CustomerTabFragmentAdapter customerTabFragmentAdapter;
    public OnFragmentInteractionListener mListener;

    public SubCategory_Automotive_Repair_Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SubCategory_Automotive_Repair_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SubCategory_Automotive_Repair_Fragment newInstance(String param1, String param2) {
        SubCategory_Automotive_Repair_Fragment fragment = new SubCategory_Automotive_Repair_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            autosublist=(String[]) getArguments().getSerializable("automotivesublist");
            mainindex=getArguments().getInt("mainindex");
            Log.e("frag sublist",""+autosublist.length);
            Log.e("frag main list index",""+mainindex);

        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_sub_category__automotive__repair_, container, false);
        listView=(ListView)v.findViewById(R.id.listview);
        arrayAdapter=new ArrayAdapter(getContext(),R.layout.subcategory_list_automotive_layout,R.id.textview,autosublist);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(), "ArrayList Postion"+position+"\n"+mainindex, Toast.LENGTH_SHORT).show();
           // getActivity().getSupportFragmentManager().beginTransaction().add(R.id.container,new Post_Requirement_Fragment()).addToBackStack("SubCategory_Automotive_Repair_Fragment").commit();
            //new CustomerTabFragmentAdapter(getChildFragmentManager(),1);
                //Intent i=new Intent(getActivity(),Post_Requirement_Fragment.class);
                 //startActivity(i);
                tablayout=(TabLayout)getActivity().findViewById(R.id.tablayout);
            tab=tablayout.getTabAt(1);
                tab.select();
                listviewclicked=true;
            }
        });

        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if(listviewclicked)
                {
                   //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container,)
                   // Toast.makeText(getActivity(), ""+listviewclicked, Toast.LENGTH_SHORT).show();
                }
                else
                {
                   // Toast.makeText(getActivity(), ""+listviewclicked, Toast.LENGTH_SHORT).show();
                }
            }
        });
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
           // mListener.onFragmentInteraction(boolean listviewstatus);
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();



    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
