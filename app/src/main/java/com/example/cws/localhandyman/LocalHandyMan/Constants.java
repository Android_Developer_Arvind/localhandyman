package com.example.cws.localhandyman.LocalHandyMan;

import com.example.cws.localhandyman.R;

/**
 * Created by cws on 9/4/18.
 */

public class Constants {
   public static int mainserviceiconarray[]={R.drawable.autorepair,R.drawable.legalservice,R.drawable.household1,R.drawable.personalservices1,R.drawable.tutors1,R.drawable.realestate1,R.drawable.businessservice,R.drawable.repairmaintenance,R.drawable.lessionimage,R.drawable.weddingplanning};
    public static String maincategory[]={"Automotive Repair","Legal Services","Household Services","Personal & More","Tutors Services","Real Estate Service","Business Service","Repair & Maintenance","Lession & Hobbies","Events & Wedding"};
    public static  String data[]={"Home","Posted Job","Notification","My Profile","Contact Us","About Us","Cancelation Policy","Faq","Logout"};
    public static int[] imageresource= new int[]{R.drawable.home,R.drawable.my_fav_green,R.drawable.alert_icon,R.drawable.user_icon,R.drawable.contacts,R.drawable.about,R.drawable.privacy_drawer,R.drawable.faq,R.drawable.logout};
    public static String[] radius={"2km","5km","8km","10km","15km","20km","25km","30km","35km","40km","40+ km"};
    // String[] maincategory={"Selecte Category ","Automotive Repair","Legal","HouseHold Services","Personal & More","Tutors","Real Estate Service","Business Service","Repair & Maintenance","Lession & Hobbies","Events & Wedding"};
    public static String [] automotive_subcategor={"Car Cleaning & Servicing","Car Repair & Denting","Driver On Demand"};
    public static String[] legal_subcategory={"Divorce Lawyer","Public Lawyer","Real Estate Lawyer","Visa Agency"};
    public static String[] household_subcategory={"AC Repair","Mobile Repair","Architect","Carpenter","Chimney and Hob Repair","Electrician","House Painters","Modular kitchen","Laptop Repair","Microwave Repair","Pest Control","Plumber","Bathroom Deep Cleaning","Carpet Cleaning","Home Deep Cleaning","Refrigerator Repair","RO & Water Purifier Repair","Water Proofing","Television Repair","Washing Repair","Furniture Repair","Ladies Salon At Home", "Men Salon At Home","Birthday Party Planner","Wedding Planner","Mover's & Packer's"};
    public static  String[] personel_subcategory={"Monthly Tiffin Services","Portfolio & Photography"};
    public static String [] tutors_subcategory={"Spanish Lession","French Lession","Classical Dance & Music Lession","Sign Language","Musical Instrument Lession","Home Tutor Upto 12 Class"};
    public static String [] realestate_subcategory={"Buy New & Old Home","Room On Rent Services"};
    public static String [] repairmaintenance_subcategory={"Home Repair And Contruction"};
    public static String [] businessservice_subcategory={"CA For Small Business","CCTV Cameras Installation","CA/CS For Company Registration","Corporate Party Planner","Insurance Agent","Web Designer & Development"};
    public static String [] lession_subcategory={"Drum Classes","Guitar Classes","KeyBoard Classes","Salsa Dance Class","Zumba Fitness Dance","Hip Pop Dance Classes"};
    public static String [] event_subcategory={"Mehendi Artists","Corporate Event Planer","DJ","Live Musician","Pre Wedding Shoot","Wedding Caterers","Bridal Makeup Artist","Wedding Photographer"};
}
