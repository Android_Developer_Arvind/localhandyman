package com.example.cws.localhandyman.LocalHandyMan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.cws.localhandyman.R;

public class Main2Activity extends AppCompatActivity {
  TextView customer,service_provider;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        customer=(TextView)findViewById(R.id.customer_textview);
        service_provider=(TextView)findViewById(R.id.service_provider_textview);
        customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(Main2Activity.this,CustomerHomeScreenActivity.class);
                startActivity(i);

            }
        });
        service_provider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(Main2Activity.this,ServiceProviderHomeScreenActivity.class);
                startActivity(i);

            }
        });
    }
}
