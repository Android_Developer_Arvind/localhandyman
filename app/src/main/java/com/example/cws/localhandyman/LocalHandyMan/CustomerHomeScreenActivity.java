package com.example.cws.localhandyman.LocalHandyMan;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cws.localhandyman.R;

public class CustomerHomeScreenActivity extends AppCompatActivity implements View.OnClickListener {
     TextView signup,forgetpassword;
     EditText email,password;
    String useremail,userpassword;
     TextView loginbtn;
    SharedPreferences sharedPreferences;
    ImageView fblogin,gmaillogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        email=(EditText)findViewById(R.id.customer_email);
        password=(EditText)findViewById(R.id.customer_password);
        loginbtn=(TextView)findViewById(R.id.loginbtn);
        fblogin=(ImageView)findViewById(R.id.facebookimage);
        gmaillogin=(ImageView)findViewById(R.id.gmailimage);
        signup=(TextView)findViewById(R.id.signupbtn);
        forgetpassword=(TextView)findViewById(R.id.forgetpassword);
        signup.setOnClickListener(this);
        forgetpassword.setOnClickListener(this);
        loginbtn.setOnClickListener(this);
        fblogin.setOnClickListener(this);
        gmaillogin.setOnClickListener(this);
    }



    @Override
    public void onClick(View view) {
  switch (view.getId()) {
      case R.id.loginbtn:
          //Toast.makeText(this, "Please Wait", Toast.LENGTH_SHORT).show();
          sharedPreferences = getSharedPreferences("User_Detail", 0);
          // Log.e("username",sharedPreferences.getString("name",null));
          // Log.e("email",sharedPreferences.getString("email",null));
          //Log.e("password",sharedPreferences.getString("password",null));
          useremail = sharedPreferences.getString("email", null);
          userpassword = sharedPreferences.getString("password", null);
          Log.e("useremail", "" + useremail);
          Log.e("psswd", "" + userpassword);

  if(!(email.getText().toString().trim().equals(" ") || password.getText().toString().trim().equals(" ")))
      {
          if (userpassword != null && useremail != null) {
              if (email.getText().toString().trim().equals(useremail) && password.getText().toString().trim().equals(userpassword)) {
                  Toast.makeText(this, "Login Successfull", Toast.LENGTH_SHORT).show();
                  Intent i = new Intent(CustomerHomeScreenActivity.this, CustomerMainScreen.class);
                  startActivity(i);
              } else {
                  Toast.makeText(this, "Login Failed", Toast.LENGTH_SHORT).show();
              }
          } else {
              Toast.makeText(this, "Please Sign Up First ", Toast.LENGTH_SHORT).show();
          }
  }
          else
      {
          Toast.makeText(this, "Please Enter Detail ", Toast.LENGTH_SHORT).show();
      }
          break;
      case R.id.facebookimage:
          Toast.makeText(this, "Please Wait", Toast.LENGTH_SHORT).show();
          break;
      case  R.id.gmailimage:
          Toast.makeText(this, "Please Wait", Toast.LENGTH_SHORT).show();
          break;
      case R.id.signupbtn:
          Intent i=new Intent(CustomerHomeScreenActivity.this,CustomerSignUp.class);
          startActivity(i);
          break;
      case  R.id.forgetpassword:
          Intent i1=new Intent(CustomerHomeScreenActivity.this,ForgetPassword.class);
          startActivity(i1);
          break;
  }
    }
}
