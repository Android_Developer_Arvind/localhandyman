package com.example.cws.localhandyman.ConstantHelperMethods;

import android.util.Patterns;

import java.util.regex.Pattern;

/**
 * Created by cws on 4/4/18.
 */

public class HelperClass
{

public static boolean isValidEmail(String email)
{
    if(Patterns.EMAIL_ADDRESS.matcher(email).matches())
        return true;
    else
        return false;
}
    public static boolean isValidPhone(String phone)
    {
        if(Patterns.PHONE.matcher(phone).matches())
            return true;
        else
            return false;
    }

}
