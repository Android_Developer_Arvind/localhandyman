package com.example.cws.localhandyman.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.cws.localhandyman.TabFragment.Post_Requirement_Fragment;
import com.example.cws.localhandyman.TabFragment.Service_Fragment;

/**
 * Created by cws on 4/4/18.
 */

public class CustomerTabFragmentAdapter extends FragmentStatePagerAdapter {
    int tabcount;

    public CustomerTabFragmentAdapter(FragmentManager fm,int tabcount) {
        super(fm);
        this.tabcount=tabcount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                Service_Fragment fragment1=new Service_Fragment();
                return fragment1;

            case 1:
                Post_Requirement_Fragment fragment2=new Post_Requirement_Fragment();
                 return fragment2;
            default:
                return null;
        }


    }

    @Override
    public int getCount() {
        return tabcount;
    }
}
