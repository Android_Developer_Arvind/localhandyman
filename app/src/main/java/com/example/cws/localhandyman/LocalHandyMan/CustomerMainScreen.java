package com.example.cws.localhandyman.LocalHandyMan;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;


import com.example.cws.localhandyman.Adapter.ListViewAdapter;
import com.example.cws.localhandyman.R;
import com.example.cws.localhandyman.Adapter.CustomerTabFragmentAdapter;
import com.example.cws.localhandyman.TabFragment.Post_Requirement_Fragment;
import com.example.cws.localhandyman.TabFragment.Service_Fragment;
import com.example.cws.localhandyman.TabFragment.SubCategory_Automotive_Repair_Fragment;
import com.example.cws.localhandyman.TabFragment.SubCategory_Business_Services_Fragment;
import com.example.cws.localhandyman.TabFragment.SubCategory_EventsWedding_Fragment;
import com.example.cws.localhandyman.TabFragment.SubCategory_HouseHold_Services_Fragment;
import com.example.cws.localhandyman.TabFragment.SubCategory_Legal_Services_Fragment;
import com.example.cws.localhandyman.TabFragment.SubCategory_LessionHobbies_Fragment;
import com.example.cws.localhandyman.TabFragment.SubCategory_Personel_Service_Fragment;
import com.example.cws.localhandyman.TabFragment.SubCategory_RealEstate_Services_Fragment;
import com.example.cws.localhandyman.TabFragment.SubCategory_RepairMaintenance_Fragment;
import com.example.cws.localhandyman.TabFragment.SubCategory_Tutors_Services_Fragment;

import java.util.ArrayList;

public class CustomerMainScreen extends FragmentActivity implements Service_Fragment.OnFragmentInteractionListener,Post_Requirement_Fragment.OnFragmentInteractionListener,SubCategory_Automotive_Repair_Fragment.OnFragmentInteractionListener,SubCategory_Legal_Services_Fragment.OnFragmentInteractionListener,SubCategory_HouseHold_Services_Fragment.OnFragmentInteractionListener,SubCategory_Personel_Service_Fragment.OnFragmentInteractionListener,SubCategory_Tutors_Services_Fragment.OnFragmentInteractionListener,SubCategory_RealEstate_Services_Fragment.OnFragmentInteractionListener,SubCategory_Business_Services_Fragment.OnFragmentInteractionListener,SubCategory_RepairMaintenance_Fragment.OnFragmentInteractionListener,SubCategory_LessionHobbies_Fragment.OnFragmentInteractionListener,SubCategory_EventsWedding_Fragment.OnFragmentInteractionListener {
ImageView togglebtn,locationicon;
    TabLayout tabLayout;
    FrameLayout container;
    DrawerLayout drawerLayout;
    ListView listView;
    ArrayList<String> dataitem,mainServiceList;
    ArrayList<Drawable> iconlist,mainServiceIconList;
    RecyclerView gridview;
    RecyclerView.LayoutManager layoutManager;
    GridLayoutManager gridlayoutmanger;
    FragmentManager fm;
    FragmentTransaction ft;
    int mainserviceiconarray[]={R.drawable.autorepair,R.drawable.legalservice,R.drawable.household1,R.drawable.personalservices1,R.drawable.tutors1,R.drawable.realestate,R.drawable.businessservice,R.drawable.repairmaintenance1,R.drawable.lession,R.drawable.events,R.drawable.events,R.drawable.events,R.drawable.events,R.drawable.events,R.drawable.events};
    String maincategory[]={"Automotive Repair","Legal","HouseHold Services","Personal & More","Tutors","Real Estate Service","Business Service","Repair & Maintenance","Lession & Hobbies","Events & Wedding","A","B","C","D","E"};
    String data[]={"Home","Posted Job","Notification","My Profile","Contact Us","About Us","Cancelation Policy","Faq","Logout"};
    int[] imageresource= new int[]{R.drawable.home,R.drawable.my_fav_green,R.drawable.notification1,R.drawable.usericon1,R.drawable.contacts,R.drawable.about,R.drawable.privacy_drawer,R.drawable.faq,R.drawable.logout};

    ArrayAdapter<String> arrayAdapter;

    ListViewAdapter listViewAdapter;

    CustomerTabFragmentAdapter customerTabFragmentAdapter;
    int r;

    @Override
    protected void onResume() {
        super.onResume();
        customerTabFragmentAdapter=new CustomerTabFragmentAdapter(getSupportFragmentManager(),tabLayout.getTabCount());

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.e("tab position",""+tab.getPosition());
                switch (tab.getPosition())
                {
                    case 0:
                        fm=getSupportFragmentManager();
                        ft=fm.beginTransaction();
                        ft.add(R.id.container,new Service_Fragment());
                        ft.commit();
                        break;
                    case 1:
                        fm=getSupportFragmentManager();
                        ft=fm.beginTransaction();
                        ft.replace(R.id.container,new Post_Requirement_Fragment());
                        ft.commit();
                        break;
                    default:
                        break;

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fm=getSupportFragmentManager();
        ft=fm.beginTransaction();
        ft.add(R.id.container,new Service_Fragment());
        ft.commit();
        setContentView(R.layout.activity_customer_main_screen);
        togglebtn=(ImageView)findViewById(R.id.togglebtn);
        locationicon=(ImageView)findViewById(R.id.locationicon);
        tabLayout=(TabLayout)findViewById(R.id.tablayout);
        layoutManager=new LinearLayoutManager(this);
        container=(FrameLayout)findViewById(R.id.container);
        drawerLayout=(DrawerLayout)findViewById(R.id.drawerlayout);
        listView=(ListView)findViewById(R.id.listview);
        //gridview=(RecyclerView) findViewById(R.id.gridview);
//        gridview.setLayoutManager(layoutManager);
        iconlist=new ArrayList<Drawable>();
        mainServiceList=new ArrayList<>();
        mainServiceIconList=new ArrayList<>();
        dataitem=new ArrayList<>();
        for(int i=0;i<data.length;i++)
        {
         dataitem.add(i,data[i]);
            iconlist.add(i,getDrawable(imageresource[i]));
        }
        Log.e("list drawer size",""+dataitem.size());
        Log.e("list drawer size",""+iconlist.size());
        //for(int i=0;i<maincategory.length;i++)
      //  {
       //     mainServiceList.add(i,maincategory[i]);
       //     mainServiceIconList.add(i,getDrawable(mainserviceiconarray[i]));
       // }
      //  Log.e("list size",""+mainServiceList.size());
       // Log.e("list size",""+mainServiceIconList.size());
        addDataToDrawerList();

        customerTabFragmentAdapter=new CustomerTabFragmentAdapter(getSupportFragmentManager(),tabLayout.getTabCount());
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(CustomerMainScreen.this, ""+position, Toast.LENGTH_SHORT).show();
                Log.e("view",""+view);
                Log.e("adapteview",""+parent);
                Log.e("position",""+position);
                Log.e("view id",""+id);
            }
        });
        drawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
   togglebtn.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if(drawerLayout.isDrawerOpen(listView))
        {
            drawerLayout.closeDrawer(listView);
        }
        else if(!drawerLayout.isDrawerOpen(listView))
        {
            drawerLayout.openDrawer(listView);
        }
    }
});


    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
    private void addDataToDrawerList()
    {
        listViewAdapter=new ListViewAdapter(CustomerMainScreen.this,dataitem,iconlist);
        listView.setAdapter(listViewAdapter);
        listViewAdapter.notifyDataSetChanged();
    }
    // this is not used
    /*
    private void addDataToRecyclerView()
    {
        customerloginrecyclerviewadapter=new CustomerloginRecyclerDataAdapter(CustomerMainScreen.this,mainServiceList,mainServiceIconList);
        gridview.setAdapter(customerloginrecyclerviewadapter);
        customerloginrecyclerviewadapter.notifyDataSetChanged();
    }
    */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(tabLayout.getTabAt(1).isSelected())
        {
            tabLayout.getTabAt(0).select();

        }
    }
}
