package com.example.cws.localhandyman.TabFragment;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.cws.localhandyman.Adapter.ServiceFragmentAdapter;
import com.example.cws.localhandyman.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Service_Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Service_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Service_Fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    RecyclerView recyclerView;
    RecyclerView.LayoutManager linear;
    ArrayList<String> dataitem,mainServiceList;
    ArrayList<Drawable> iconlist,mainServiceIconList;
    ServiceFragmentAdapter serviceFragmentAdapter;
    int mainserviceiconarray[]={R.drawable.autorepair,R.drawable.legalservice,R.drawable.household1,R.drawable.personalservices1,R.drawable.tutors1,R.drawable.realestate1,R.drawable.businessservice,R.drawable.repairmaintenance,R.drawable.lessionimage,R.drawable.weddingplanning};
    String maincategory[]={"Automotive Repair","Legal Services","Household Services","Personal & More","Tutors Services","Real Estate Service","Business Service","Repair & Maintenance","Lession & Hobbies","Events & Wedding"};
    String data[]={"Home","Posted Job","Notification","My Profile","Contact Us","About Us","Cancelation Policy","Faq","Logout"};
    int[] imageresource= new int[]{R.drawable.home,R.drawable.my_fav_green,R.drawable.alert_icon,R.drawable.user_icon,R.drawable.contacts,R.drawable.about,R.drawable.privacy_drawer,R.drawable.faq,R.drawable.logout};
    String[] radius={"2km","5km","8km","10km","15km","20km","25km","30km","35km","40km","40+ km"};
   // String[] maincategory={"Selecte Category ","Automotive Repair","Legal","HouseHold Services","Personal & More","Tutors","Real Estate Service","Business Service","Repair & Maintenance","Lession & Hobbies","Events & Wedding"};
    String [] automotive_subcategor={"Car Cleaning & Servicing","Car Repair & Denting","Driver On Demand"};
    String[] legal_subcategory={"Divorce Lawyer","Public Lawyer","Real Estate Lawyer","Visa Agency"};
    String[] household_subcategory={"AC Repair","Mobile Repair","Architect","Carpenter","Chimney and Hob Repair","Electrician","House Painters","Modular kitchen","Laptop Repair","Microwave Repair","Pest Control","Plumber","Bathroom Deep Cleaning","Carpet Cleaning","Home Deep Cleaning","Refrigerator Repair","RO & Water Purifier Repair","Water Proofing","Television Repair","Washing Repair","Furniture Repair","Ladies Salon At Home", "Men Salon At Home","Birthday Party Planner","Wedding Planner","Mover's & Packer's"};
    String[] personel_subcategory={"Monthly Tiffin Services","Portfolio & Photography"};
    String [] tutors_subcategory={"Spanish Lession","French Lession","Classical Dance & Music Lession","Sign Language","Musical Instrument Lession","Home Tutor Upto 12 Class"};
    String [] realestate_subcategory={"Buy New & Old Home","Room On Rent Services"};
    String [] repairmaintenance_subcategory={"Home Repair And Contruction"};
    String [] businessservice_subcategory={"CA For Small Business","CCTV Cameras Installation","CA/CS For Company Registration","Corporate Party Planner","Insurance Agent","Web Designer & Development"};
    String [] lession_subcategory={"Drum Classes","Guitar Classes","KeyBoard Classes","Salsa Dance Class","Zumba Fitness Dance","Hip Pop Dance Classes"};
    String [] event_subcategory={"Mehendi Artists","Corporate Event Planer","DJ","Live Musician","Pre Wedding Shoot","Wedding Caterers","Bridal Makeup Artist","Wedding Photographer"};
    ArrayList<String[]> maincategorysublist;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Service_Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Service_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Service_Fragment newInstance(String param1, String param2) {
        Service_Fragment fragment = new Service_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        iconlist=new ArrayList<Drawable>();
        mainServiceList=new ArrayList<String>();
        mainServiceIconList=new ArrayList<Drawable>();
        dataitem=new ArrayList<String>();
        for(int i=0;i<maincategory.length;i++)
        {
            mainServiceList.add(i,maincategory[i]);
            mainServiceIconList.add(i,getResources().getDrawable(mainserviceiconarray[i]));
        }
        Log.e("list frag ment size",""+mainServiceList.size());
        Log.e("list size",""+mainServiceIconList.size());
        maincategorysublist=new ArrayList<>();
        maincategorysublist.add(0,automotive_subcategor);
        maincategorysublist.add(1,legal_subcategory);
        maincategorysublist.add(2,household_subcategory);
        maincategorysublist.add(3,personel_subcategory);
        maincategorysublist.add(4,tutors_subcategory);
        maincategorysublist.add(5,realestate_subcategory);
        maincategorysublist.add(6,businessservice_subcategory);
        maincategorysublist.add(7,repairmaintenance_subcategory);
        maincategorysublist.add(8,lession_subcategory);
        maincategorysublist.add(9,event_subcategory);
        Log.e("new list size",""+maincategorysublist.size());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_service_, container, false);
        recyclerView=(RecyclerView)v.findViewById(R.id.recyclerview);
        linear=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2,GridLayoutManager.VERTICAL,false));
        serviceFragmentAdapter=new ServiceFragmentAdapter(getActivity(),mainServiceList,mainServiceIconList,maincategorysublist);
        recyclerView.setAdapter(serviceFragmentAdapter);
        serviceFragmentAdapter.notifyDataSetChanged();



        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
