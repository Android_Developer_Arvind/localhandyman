package com.example.cws.localhandyman.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cws.localhandyman.LocalHandyMan.CustomerMainScreen;
import com.example.cws.localhandyman.R;
import com.example.cws.localhandyman.TabFragment.SubCategory_Automotive_Repair_Fragment;
import com.example.cws.localhandyman.TabFragment.SubCategory_Business_Services_Fragment;
import com.example.cws.localhandyman.TabFragment.SubCategory_EventsWedding_Fragment;
import com.example.cws.localhandyman.TabFragment.SubCategory_HouseHold_Services_Fragment;
import com.example.cws.localhandyman.TabFragment.SubCategory_Legal_Services_Fragment;
import com.example.cws.localhandyman.TabFragment.SubCategory_LessionHobbies_Fragment;
import com.example.cws.localhandyman.TabFragment.SubCategory_Personel_Service_Fragment;
import com.example.cws.localhandyman.TabFragment.SubCategory_RealEstate_Services_Fragment;
import com.example.cws.localhandyman.TabFragment.SubCategory_RepairMaintenance_Fragment;
import com.example.cws.localhandyman.TabFragment.SubCategory_Tutors_Services_Fragment;

import java.util.ArrayList;

/**
 * Created by cws on 6/4/18.
 */

public class ServiceFragmentAdapter extends RecyclerView.Adapter<ServiceFragmentAdapter.ViewHolder>
{
     Context context;
    ArrayList<String[]> maincategorysublist;
    ArrayList<String> servicedatalist;
    ArrayList<Drawable> serviceiconlist;
    LayoutInflater inflater;
    public ServiceFragmentAdapter(Context context,ArrayList<String> servicedatalist,ArrayList<Drawable> serviceiconlist,ArrayList<String[]> maincategorysublist) {
        this.context=context;
        this.servicedatalist=servicedatalist;
        this.serviceiconlist=serviceiconlist;
        this.maincategorysublist=maincategorysublist;
        Log.e("list frag adapter size",""+servicedatalist.size());
        Log.e("list adapter size",""+serviceiconlist.size());
        Log.e("list adapter size",""+maincategorysublist.size());
        Log.e("list element at","0"+maincategorysublist.get(0).length);
        Log.e("list element at","1"+maincategorysublist.get(1).length);
        Log.e("list element at","2"+maincategorysublist.get(2).length);
        Log.e("list element at","0"+servicedatalist.get(0));
        Log.e("list element at","1"+servicedatalist.get(1));
        Log.e("list element at","2"+servicedatalist.get(2));
    }

    @Override
    public ServiceFragmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v=inflater.inflate(R.layout.customerloginservicesdatalayout,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ServiceFragmentAdapter.ViewHolder holder, final int position) {
        //holder.imageView.setImageDrawable(serviceiconlist.get(position));
        holder.imageView.setBackgroundDrawable(serviceiconlist.get(position));
        holder.textView.setText(servicedatalist.get(position));

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, "Please Wait", Toast.LENGTH_SHORT).show();
                CustomerMainScreen activity = (CustomerMainScreen) v.getContext();
                switch (position)
                {
                    case 0:
                        SubCategory_Automotive_Repair_Fragment subCategory_automotive_repair_fragment=new SubCategory_Automotive_Repair_Fragment();
                        Bundle b=new Bundle();
                         b.putSerializable("automotivesublist",maincategorysublist.get(position));
                        b.putInt("mainindex",position);
                        SubCategory_Automotive_Repair_Fragment subCategoryAutomotiveRepairFragment=new SubCategory_Automotive_Repair_Fragment();
                        subCategory_automotive_repair_fragment.setArguments(b);
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.container,subCategory_automotive_repair_fragment).addToBackStack("subCategory_automotive_repair_fragment").commit();
                        break;
                    case 1:
                        SubCategory_Legal_Services_Fragment subCategory_legal_services_fragment=new SubCategory_Legal_Services_Fragment();
                        Bundle b1=new Bundle();
                        b1.putInt("mainindex",position);
                        b1.putSerializable("legalsublist",maincategorysublist.get(position));
                        //SubCategory_Legal_Services_Fragment subCategory_legal_services_fragment1l=new SubCategory_Legal_Services_Fragment();
                        subCategory_legal_services_fragment.setArguments(b1);
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.container,subCategory_legal_services_fragment).addToBackStack("subCategory_legal_services_fragment").commit();
                        break;
                    case 2:
                        SubCategory_HouseHold_Services_Fragment subCategory_houseHold_services_fragment=new SubCategory_HouseHold_Services_Fragment();
                        Bundle b2=new Bundle();
                        //SubCategory_HouseHold_Services_Fragment subCategory_houseHold_services_fragment1=new SubCategory_HouseHold_Services_Fragment();
                        b2.putSerializable("householdsublist",maincategorysublist.get(position));
                        b2.putInt("mainindex",position);
                        subCategory_houseHold_services_fragment.setArguments(b2);
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.container,subCategory_houseHold_services_fragment).addToBackStack("subCategory_houseHold_services_fragment").commit();
                        break;
                    case 3:
                        SubCategory_Personel_Service_Fragment subCategory_personel_service_fragment=new SubCategory_Personel_Service_Fragment();
                        Bundle b3=new Bundle();
                        b3.putSerializable("personelservicesublist",maincategorysublist.get(position));
                        b3.putInt("mainindex",position);
                        subCategory_personel_service_fragment.setArguments(b3);
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.container,subCategory_personel_service_fragment).addToBackStack("subCategory_personel_service_fragment").commit();
                        break;
                    case 4:
                        SubCategory_Tutors_Services_Fragment subCategory_tutors_services_fragment=new SubCategory_Tutors_Services_Fragment();
                        Bundle b4=new Bundle();
                        b4.putInt("mainindex",position);
                        b4.putSerializable("tutorservicesublist",maincategorysublist.get(position));
                        subCategory_tutors_services_fragment.setArguments(b4);
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.container,subCategory_tutors_services_fragment).addToBackStack("subCategory_tutors_services_fragment").commit();
                        break;
                    case 5:
                        SubCategory_RealEstate_Services_Fragment subCategory_realEstate_services_fragment=new SubCategory_RealEstate_Services_Fragment();
                        Bundle b5=new Bundle();
                        b5.putInt("mainindex",position);
                        b5.putSerializable("realestateservicesublist",maincategorysublist.get(position));
                        subCategory_realEstate_services_fragment.setArguments(b5);
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.container,subCategory_realEstate_services_fragment).addToBackStack("subCategory_realEstate_services_fragment").commit();
                        break;
                    case 6:
                        SubCategory_Business_Services_Fragment subCategory_business_services_fragment=new SubCategory_Business_Services_Fragment();
                        Bundle b6=new Bundle();
                        b6.putSerializable("businessservicesublist",maincategorysublist.get(position));
                        b6.putInt("mainindex",position);
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.container,subCategory_business_services_fragment).addToBackStack("subCategory_business_services_fragment").commit();
                        subCategory_business_services_fragment.setArguments(b6);
                        break;
                    case 7:
                        SubCategory_RepairMaintenance_Fragment subCategory_repairMaintenance_fragment=new SubCategory_RepairMaintenance_Fragment();
                        Bundle b7=new Bundle();
                        b7.putSerializable("repairservicesublist",maincategorysublist.get(position));
                        b7.putInt("mainindex",position);
                        subCategory_repairMaintenance_fragment.setArguments(b7);
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.container,subCategory_repairMaintenance_fragment).addToBackStack("subCategory_repairMaintenance_fragment").commit();
                        break;
                    case 8:
                        SubCategory_LessionHobbies_Fragment subCategory_lessionHobbies_fragment=new SubCategory_LessionHobbies_Fragment();
                        Bundle b8=new Bundle();
                        b8.putInt("mainindex",position);
                        b8.putSerializable("lessionservicesublist",maincategorysublist.get(position));
                        subCategory_lessionHobbies_fragment.setArguments(b8);
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.container,subCategory_lessionHobbies_fragment).addToBackStack("subCategory_lessionHobbies_fragment").commit();
                        break;
                    case 9:
                       SubCategory_EventsWedding_Fragment subCategory_eventsWedding_fragment=new SubCategory_EventsWedding_Fragment();
                        Bundle b9=new Bundle();
                        b9.putInt("mainindex",position);
                        b9.putSerializable("eventservicesublist",maincategorysublist.get(position));
                        subCategory_eventsWedding_fragment.setArguments(b9);
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.container,subCategory_eventsWedding_fragment).addToBackStack("subCategory_eventsWedding_fragment").commit();
                        break;

                    default:
                        Toast.makeText(activity, "Please Select Item", Toast.LENGTH_SHORT).show();

                }



            }
        });
    }

    @Override
    public int getItemCount() {

        return servicedatalist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        public ViewHolder(View itemView) {
            super(itemView);
            imageView=(ImageView)itemView.findViewById(R.id.serviceimageview);
            textView=(TextView)itemView.findViewById(R.id.servicetextview);

        }
    }
}
